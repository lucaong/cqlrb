require 'cqlrb/version'
require 'cassandra'

class Cqlrb
  def initialize(options)
    cluster = Cassandra.cluster(options)
    @connection = cluster.connect
  end

  def eval(statements)
    statements.split(';').reduce('') do |_, statement|
      @connection.execute(statement)
    end
  end

  def eval_to_json(statements, options = {})
    eval(statements).map { |result| JSON.pretty_generate(result) }
  end
end

module Cassandra
  class UDT
    def to_json(options = {})
      JSON.pretty_generate(to_h, options)
    end
  end
end
