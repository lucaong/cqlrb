# Cqlrb

Proof of concept of CQL console outputting JSON

## Installation

Make sure you have a recent Ruby version (`>= 2.0.0`) and run:

```
$ gem install bundler
$ bundle install
$ rake install
```

## Usage

Just check `cqlrb --help`

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

